site_name: DeMi Docs
site_description: Detector Microservices for ATLAS ITk Pixel System Test Documentation
site_author: Gerhard Brandt et al.
site_url: https://atlas-itk-pixel-systemtest.docs.cern.ch/itk-demo-sw/
site_dir: public

repo_name: itk-demo-sw
repo_url: https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw
edit_uri: https://gitlab.cern.ch/groups/atlas-itk-pixel-systemtest/itk-demo-sw/-/wikis/home

theme:
  name: material
  icon:
    repo: fontawesome/brands/gitlab

  palette:
    - media: "(prefers-color-scheme: light)"
      scheme: default
      toggle:
        icon: material/brightness-7
        name: Switch to dark mode
      primary: deep orange

    # Palette toggle for dark mode
    - media: "(prefers-color-scheme: dark)"
      scheme: slate
      toggle:
        icon: material/brightness-4
        name: Switch to light mode
      primary: orange

  # collapse_navigation: false
  hljs_style:
    - gitlab
  hljs_languages:
    - yaml

markdown_extensions:
  # Python Markdown
  - abbr
  - admonition
  - attr_list
  - def_list
  - footnotes
  - md_in_html
  - toc:
      marker: [[_TOC_]]
      permalink: true
      slugify: !!python/object/apply:pymdownx.slugs.slugify
        kwds:
          case: lower
  - tables
  # Python Markdown Extensions
  - pymdownx.arithmatex:
      generic: true
  - pymdownx.details
  - pymdownx.betterem
  - pymdownx.caret
  - pymdownx.mark
  - pymdownx.tilde
  - pymdownx.highlight:
      use_pygments: true
      pygments_lang_class: true
      auto_title: true
      linenums_style: pymdownx-inline
      anchor_linenums: true
      line_spans: __span
  - pymdownx.inlinehilite
  - pymdownx.keys
  - pymdownx.smartsymbols
  - pymdownx.snippets
  - pymdownx.superfences:
      custom_fences:
        - name: mermaid
          class: mermaid
          format: !!python/name:pymdownx.superfences.fence_code_format
  - pymdownx.tabbed:
      alternate_style: true
  - pymdownx.tasklist:
      custom_checkbox: true

plugins:
  - search
  - multirepo:
      cleanup: false
      keep_docs_dir: true
      nav_repos:
        - name: docs
          import_url: https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/docs?branch=master
          imports: [/README.md]
        - name: itk-demo-dashboard
          import_url: https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-dashboard?branch=master
          imports: [/README.md]
        - name: itk-demo-felix
          import_url: https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-felix?branch=main
          imports: [/README.md, base-image/README.md]
        - name: itk-demo-optoboard
          import_url: https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-optoboard?branch=master
          imports: [/README.md]
        - name: opto-stack
          import_url: https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/deployments/opto-stack?branch=master
          imports: [/README.md]
        - name: itk-demo-configdb
          import_url: https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-configdb?branch=master
          imports: [/README.md]
        - name: configdb-server
          import_url: https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/pypi-packages/configdb-server?branch=master
          imports: [/README.md]
        - name: itk-demo-registry
          import_url: https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-registry?branch=master
          imports: [/README.md]
        - name: tutorial
          import_url: https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/deployments/tutorial?branch=master
          imports: [/README.md]
        - name: ext-tutorial
          import_url: https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/deployments/tutorial.git?branch=extended_tutorial
          imports: [/README.md]
        - name: pyconfigdb
          import_url: https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-configdb?branch=master
          imports: [pyconfigdb/README.md]
        - name: celery-middleware
          import_url: https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-celery?branch=master
          imports: [middleware/README.md]
  #- awesome-pages
        

nav:
  - Overview: index.md
  - FAQ: faq.md
  - Deployments:
    - Deployments: tutorials/deployment.md
  - Tutorials:
    - Tutorial stack: tutorial/README.md
    - Extended tutorial stack: ext-tutorial/README.md
    - Access from outside CERN: tutorials/remote.md
    - Runkeys: tutorials/runkey.md
    - configdb-server: tutorials/configdb-server.md
    - celery-middleware: celery-middleware/middleware/README.md
  - Microservices:
    - itk-demo-dashboard: itk-demo-dashboard/README.md
    - itk-demo-felix: 
      - Overview: itk-demo-felix/README.md
      - base-image: itk-demo-felix/base-image/README.md
    - itk-demo-optoboard: itk-demo-optoboard/README.md
    - itk-demo-configdb: itk-demo-configdb/README.md
    - itk-demo-registry: itk-demo-registry/README.md
    - Service Registry: pypi-packages/service-registry.md
  - Containers: 
    - Overview: containers/containers.md
    - Docker: containers/docker.md
  - Development:
    - Standard Repository: repository/repository.md
    - Development Containers: containers/devcontainers.md
  - Python Packages: 
    - Overview: pypi-packages/pypi-packages.md
    - configdb-server: pypi-packages/configdb-server.md
    - pyconfigdb: pyconfigdb/pyconfigdb/README.md
  - Depreceated Topics:
    - Overview: depreceated/depreceated.md  
    - JavaScript Packages: depreceated/npm-packages/npm-packages.md
    - External Packages: depreceated/external.md
    - General Related Topics: depreceated/general/general.md
  - Documentation How-To: docs.md
  - Resources: resources.md


